const axios = require('axios').default;
const express = require('express');
const app = express()
const config = require('./config.json');

app.use('/', express.static('dist'))

app.get('/character', async (req, res) => {
    res.writeHead(200, '', { 'Access-Control-Allow-Origin': "http://localhost:3000" });

    const cobaltSessionResponse = await axios.post(
        'https://auth-service.dndbeyond.com/v1/cobalt-token',
        null,
        {
            headers: {
                'cookie': 'CobaltSession=' + config.cobaltSession
            }
        });
    if (cobaltSessionResponse.status != 200) {
        res.end(500);
    }
    const cobaltToken = cobaltSessionResponse.data.token;

    const charactersPayload = {
        "characterIds": config.characterIds
    }
    const dataResponse = await axios.post(
        'https://character-service-scds.dndbeyond.com/v1/characters',
        charactersPayload,
        {
            headers: {
                'authorization': 'Bearer ' + cobaltToken
            }
        });
    if (dataResponse.status != 200) {
        res.end(500);
    }
    res.end(JSON.stringify(dataResponse.data));
});

const port = 8080;

app.listen(port, () => {
    console.log("http://localhost:" + port)
})